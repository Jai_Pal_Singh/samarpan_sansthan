<link href="css/navbar.css" rel="stylesheet"> 
 <nav class="navbar">
    <div class="container container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar3">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><img width="100%" height="200%" src="..\src\img\samarpan_sansthan.jpg" alt="Samarpan Sanathan">
        </a>
		<a class="navbar-brand" href="#"><font style="font-weight:bold;font-size:45px;padding-top: 50px;vertical-align : middle;">Samarpan Sansthan</font></a>
      </div>
      <div id="navbar3" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#">Home</a></li>
			<li><a href="#">Latest News</a></li>
			<li><a href="#">Volunteers</a></li>
			<li><a href="#">About Us</a></li>
			<li><a href="#">Contact Us</a></li>
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
    <!--/.container-fluid -->
  </nav>
